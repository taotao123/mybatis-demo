package com.mybatis.example;

import com.mybatis.example.data.SessionFactoryManager;
import com.mybatis.example.mapper.EmployeeMapper;
import com.mybatis.example.model.EmployeeInfo;
import org.apache.ibatis.session.SqlSession;

import java.util.List;

/**
 * Hello world!
 *
 */
public class Console
{
    public static void main( String[] args )
    {
        SqlSession session = SessionFactoryManager.openSession();
        EmployeeMapper userDA = session.getMapper(EmployeeMapper.class);
        List<EmployeeInfo> users = userDA.GetAll();
        for(EmployeeInfo user :users){
            System.out.println(
                    String.format("%s-%s"
                            ,user.getLastName()
                            ,user.getTitle()
                    ));
        }
    }
}
