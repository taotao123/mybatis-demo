package com.mybatis.example.model;

/**
 * Created by carrier.zhang on 12/30/2014.
 */
public class EmployeeInfo {
    private int EmployeeID;
    private String LastName;
    private String Title;

    public int getEmployeeID() {
        return EmployeeID;
    }

    public void setEmployeeID(int EmployeeID) {
        this.EmployeeID = EmployeeID;
    }

    public String getLastName() {
        return this.LastName;
    }

    public void setLastName(String LastName) {
        this.LastName = LastName;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }
}
