package com.mybatis.example.mapper;

import com.mybatis.example.model.EmployeeInfo;

import java.util.List;

/**
 * Created by carrier.zhang on 12/30/2014.
 */
public interface EmployeeMapper {
    List<EmployeeInfo> GetList(int id);
    List<EmployeeInfo> GetAll();
}
